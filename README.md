# Clean & Concise config for ESLint
Cleancise favours functional style code, enforcing the minimun syntax overhead.
This config is based on eslint-config-airbnb-base, and includes its lastest version as a dependency.
* No semicolons (except when necessary)
* 2 spaces indent
* 160 max line length
* Dangling / trailing commas for multiline objects
* Prefers destructuring assignments
* The only kind of for loops alowed are for...of loops
* The only function parameter name allowed to be mutated is `acc`
* Allows and enforces the latest ES2020 syntax

## Installation
```javascript
npm i -D eslint eslint-config-cleancise
```

## Setup
```
// .eslintrc.js
module.exports = {
  extends: [
    'eslint:recommended', // or eslint:all for the latest rules
    'cleancise',
  ],
}
```

Optional:
```
// .eslintrc.js
  ...
  env: {
    browser: true, // Only for front-end projects, if you need browser global variables
  },
  rules: {
    // Your rules
  },
  ...
```

## VSCode integration

For realtime linting and autofix on save!

This will make ESLint the default formatter of JS files in VSCode:
* Install the ESLint extension for VSCode
* Open the command palette with CMD/CTRL + SHIFT + P  and select "Preferences: Open settings (JSON)"
* Add these config options:
```
    "editor.formatOnSave": true,
    "eslint.format.enable": true,
    "[javascript]": {
        "editor.defaultFormatter": "dbaeumer.vscode-eslint"
    },
```
* Restart VSCode
module.exports = {
  extends: ['airbnb-base'],
  root: true,
  env: {
    es2022: true,
  },
  parserOptions: {
    ecmaVersion: 2022,
    sourceType: 'module',
  },
  rules: {
    // Global code style
    'semi': ['error', 'never', { beforeStatementContinuationChars: 'always' }],
    'indent': ['error', 2],
    'quotes': ['error', 'single'],
    'no-param-reassign': ['error', {
      props: false,
    }],
    'max-len': ['warn', {
      code: 160,
      tabWidth: 2,
      ignoreTrailingComments: true,
      ignoreUrls: true,
      ignoreStrings: true,
      ignoreTemplateLiterals: true,
      ignoreRegExpLiterals: true,
    }],

    // Objects
    'comma-dangle': ['error', 'always-multiline'],
    'quote-props': ['error', 'consistent-as-needed'],
    'prefer-destructuring': ['warn', {
      VariableDeclarator: {
        array: false,
        object: false,
      },
      AssignmentExpression: {
        array: false,
        object: false,
      },
    }, {
      enforceForRenamedProperties: false,
    }],
    'object-curly-newline': ['error', {
      multiline: true,
      consistent: true,
    }],
    'object-property-newline': ['error', { allowAllPropertiesOnSameLine: true }],

    // Looser rules concerning for... of loops
    'no-await-in-loop': 'off',
    'no-continue': 'off',

    // Minor tweaks
    'no-plusplus': 'off',
    'no-unused-vars': 'warn',
    'arrow-parens': ['error', 'as-needed'],
    'no-confusing-arrow': 'off',
    'consistent-return': 'off',
    'newline-per-chained-call': 'off',
    'no-nested-ternary': 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-restricted-syntax': [
      'error',
      {
        selector: 'ForInStatement',
        message: 'for..in loops iterate over the entire prototype chain, which is virtually never what you want. Use Object.{keys,values,entries}, and iterate over the resulting array.',
      },
      {
        selector: 'ForStatement',
        message: 'For loops are hard to read and mantain. Replace them with high order functions (map, reduce, find, some, any, filter...) if you can avoid side-effects. Or with `forEach` or `for... of` otherwise.',
      },
      {
        selector: 'LabeledStatement',
        message: 'Labels are a form of GOTO; using them makes code confusing and hard to maintain and understand.',
      },
      {
        selector: 'WithStatement',
        message: '`with` is disallowed in strict mode because it makes code impossible to predict and optimize.',
      },
      'SequenceExpression',
    ],
  },
}
